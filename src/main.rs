pub fn main() {
    let mut args = std::env::args();
    let cmd = args.next().unwrap();

    let path = if let Some(path) = args.next() {
        path
    } else {
        panic!("Usage: {} <path>", cmd);
    };

    let db = sled::Config::new()
        .use_compression(true)
        .path(path)
        .open()
        .unwrap();

    let all_trees = db.tree_names().into_iter();

    let mut total_sum = 0;

    for tree in all_trees {
        let mut biggest = 0;
        let mut count = 0;

        let sum = db
            .open_tree(&tree)
            .unwrap()
            .iter()
            .fold(0, |acc, e| {
                let e = e.unwrap();
                count += 1;

                if biggest < e.0.len() + e.1.len() {
                    biggest = e.0.len() + e.1.len();
                }

                acc + e.0.len() + e.1.len()
            })
            / 1024 / 1024; // convert to mb

        total_sum += sum;

        println!("\nTree: {}", String::from_utf8_lossy(&tree));
        println!("Len: {}", count);
        println!("Sum: {} MB", sum);
        println!("Biggest: {} MB", biggest / 1024 / 1024); // convert to mb
    }

    println!("\nTotal Sum: {} MB", total_sum);
}
